$(function() {
	$.getJSON('static/json/data.json', leng_text => {
		var leng = navigator.language ? navigator.language : "en"; //获取浏览器语言
		var lang_search = location.search.split("=")[1]; //获取用户选择的语言 例如url:  http://www.***.com/?leng=en
		var leng_xh = false;

		if (lang_search) {
			leng = lang_search;
		};
		if (leng.length < 2) { //如果leng的长度小于2 默认是cn
			leng = "en";
		};
		for (let la in leng_text) {
			if (leng.indexOf(la) >= 0) {
				leng = la;
				leng_xh = true;
				break;
			};
		};
		if (leng_xh == false) { // 如果没有设置该语言的翻译 默认使用cn语言
			leng = "cn";
		};
		if (leng != 'cn') { //html中已经有cn对应的文本信息，不用执行插入操作
			for (var x in leng_text[leng]) { //插入对应的文本或html元素
				$("." + x).html(leng_text[leng][x]);
			};
		};
	});
});

function myFunction() {
	document.getElementById("myDropdown").classList.toggle("show"); //下拉菜单按键
}

// window.onclick = function(event) {
// 	if (!event.target.matches('.dropbtn')) {
// 		var dropdowns = document.getElementsByClassName("dropdown-content");
// 		var i;
// 		for (i = 0; i < dropdowns.length; i++) {
// 			var openDropdown = dropdowns[i];
// 			if (openDropdown.classList.contains('show')) {
// 				openDropdown.classList.remove('show');
// 			}
// 		}
// 	}
// }
